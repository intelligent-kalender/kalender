package org.bestname.kalender.repository;

import org.bestname.kalender.entities.User;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

import static org.assertj.core.api.Assertions.assertThat;

/**
 * @author Roland Tömösközi (roland.toemoeskoezi@outlook.com)
 * Created on 05.01.2021
 */
@RunWith(SpringRunner.class)
@DataJpaTest
@ActiveProfiles("test")
public class UserRepositoryUnitTest {
    @Autowired
    private TestEntityManager entityManager; // insert an Event in the DB

    @Autowired
    private UserRepository userRepository;

    @Test
    public void save() {
        User mustermann = new User();
        mustermann.setUsername("mustermann");
        mustermann.setPassword("foobar");
        mustermann.setRole("ROLE_USER");
        mustermann.setEnabled(true);
        mustermann.setFullName("Herr Mustermann");
        mustermann.setEmail("mustermann@gmail.com");
        mustermann.setPhoneNumber("+436601234567");

        User user = userRepository.save(mustermann);
        assertThat(user.getUsername()).isEqualTo(mustermann.getUsername());
        assertThat(user.getPassword()).isEqualTo(mustermann.getPassword());
        assertThat(user.getRole()).isEqualTo(mustermann.getRole());
        assertThat(user.isEnabled()).isEqualTo(mustermann.isEnabled());
        assertThat(user.getFullName()).isEqualTo(mustermann.getFullName());
        assertThat(user.getEmail()).isEqualTo(mustermann.getEmail());
        assertThat(user.getPhoneNumber()).isEqualTo(mustermann.getPhoneNumber());
    }

    @Test
    public void findAll_if_repo_is_empty() {
        Iterable<User> users = userRepository.findAll();
        assertThat(users).isEmpty();
    }

    @Test
    public void findAll() {
        User user1 = new User();
        user1.setUsername("user1");
        user1.setPassword("password1");
        entityManager.persist(user1);

        User user2 = new User();
        user2.setUsername("user2");
        user2.setPassword("password2");
        entityManager.persist(user2);

        User user3 = new User();
        user3.setUsername("user3");
        user3.setPassword("password3");
        entityManager.persist(user3);

        Iterable<User> users = userRepository.findAll();
        assertThat(users).hasSize(3).contains(user1, user2, user3);
    }

    @Test
    public void findById() {
        User user1 = new User();
        user1.setUsername("user1");
        user1.setPassword("password1");
        entityManager.persist(user1);

        User user2 = new User();
        user2.setUsername("user2");
        user2.setPassword("password2");
        entityManager.persist(user2);

        User foundUser = userRepository.findById(user2.getId()).get();
        assertThat(foundUser).isEqualTo(user2);
    }

    @Test
    public void getUserByUsername() {
        User foobar = new User();
        foobar.setUsername("foobar");
        foobar.setPassword("foobar_pw");
        entityManager.persist(foobar);

        User found = userRepository.getUserByUsername(foobar.getUsername());
        assertThat(found.getUsername()).isEqualTo(foobar.getUsername());
    }

    @Test
    public void update_user_by_id() {
        // set
        User user1 = new User();
        user1.setUsername("user1");
        user1.setPassword("password1");
        entityManager.persist(user1);

        User user2 = new User();
        user2.setUsername("user2");
        user2.setPassword("password2");
        entityManager.persist(user2);

        // update
        User updatedUser = new User();
        updatedUser.setUsername("Updated username");
        updatedUser.setPassword("Updated password");

        User user = userRepository.findById(user2.getId()).get();
        user.setUsername(updatedUser.getUsername());
        user.setPassword(updatedUser.getPassword());
        userRepository.save(user);

        // check
        User checkUser = userRepository.findById(user2.getId()).get();
        assertThat(checkUser.getId()).isNotNull();
        assertThat(checkUser.getId()).isEqualTo(user2.getId());
        assertThat(checkUser.getUsername()).isEqualTo(updatedUser.getUsername());
        assertThat(checkUser.getPassword()).isEqualTo(updatedUser.getPassword());
    }

    @Test
    public void deleteById() {
        User user1 = new User();
        user1.setUsername("user1");
        user1.setPassword("password1");
        entityManager.persist(user1);

        User user2 = new User();
        user2.setUsername("user2");
        user2.setPassword("password2");
        entityManager.persist(user2);

        User user3 = new User();
        user3.setUsername("user3");
        user3.setPassword("password3");
        entityManager.persist(user3);

        userRepository.deleteById(user2.getId());

        Iterable<User> users = userRepository.findAll();
        assertThat(users).hasSize(2).contains(user1, user3);
    }

    @Test
    public void deleteAll() {
        User user1 = new User();
        user1.setUsername("user1");
        user1.setPassword("password1");
        entityManager.persist(user1);

        User user2 = new User();
        user2.setUsername("user2");
        user2.setPassword("password2");
        entityManager.persist(user2);

        userRepository.deleteAll();
        assertThat(userRepository.findAll()).isEmpty();
    }
}
