package org.bestname.kalender.repository;

import org.bestname.kalender.entities.Event;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Calendar;
import java.util.GregorianCalendar;

import static org.assertj.core.api.Assertions.assertThat;

/**
 * @author Roland Tömösközi (roland.toemoeskoezi@outlook.com)
 * Created on 04.01.2021
 */
@RunWith(SpringRunner.class) // bridge between Spring Boot test features and JUnit
@DataJpaTest
@ActiveProfiles("test")
public class EventRepositoryUnitTest {
    @Autowired
    private TestEntityManager entityManager; // insert an Event in the DB

    @Autowired
    private EventRepository eventRepository;

    @Test
    public void save() {
        Event zahnartzt = new Event();
        zahnartzt.setTitle("zahnartzt");
        zahnartzt.setEventStart(
                new GregorianCalendar(2020, Calendar.FEBRUARY, 5, 15, 0, 0));
        zahnartzt.setEventEnd(
                new GregorianCalendar(2020, Calendar.FEBRUARY, 5, 15, 30, 0));
        zahnartzt.setAllDay(false);

        Event event = eventRepository.save(zahnartzt);
        assertThat(event.getTitle()).isEqualTo(zahnartzt.getTitle());
        assertThat(event.getEventStart()).isEqualTo(zahnartzt.getEventStart());
        assertThat(event.getEventEnd()).isEqualTo(zahnartzt.getEventEnd());
        assertThat(event.isAllDay()).isEqualTo(zahnartzt.isAllDay());
    }

    @Test
    public void findAll_if_repo_is_empty() {
        Iterable<Event> events = eventRepository.findAll();
        assertThat(events).isEmpty();
    }

    @Test
    public void findAll() {
        Event event1 = new Event();
        event1.setTitle("First Event");
        entityManager.persist(event1);

        Event event2 = new Event();
        event2.setTitle("Second Event");
        entityManager.persist(event2);

        Event event3 = new Event();
        event3.setTitle("Third Event");
        entityManager.persist(event3);

        Iterable<Event> events = eventRepository.findAll();
        assertThat(events).hasSize(3).contains(event1, event2, event3);
    }

    @Test
    public void findById() {
        Event event1 = new Event();
        event1.setTitle("First Event");
        entityManager.persist(event1);

        Event event2 = new Event();
        event2.setTitle("Second Event");
        entityManager.persist(event2);

        Event foundEvent = eventRepository.findById(event2.getId()).get();
        assertThat(foundEvent).isEqualTo(event2);
    }

    @Test
    public void findByTitel() {
        Event friseur = new Event();
        friseur.setTitle("friseur");
        entityManager.persist(friseur);

        Event found = eventRepository.findByTitle(friseur.getTitle());
        assertThat(found.getTitle()).isEqualTo(friseur.getTitle());
    }

    @Test
    public void findByAllDay() {
        Event event1 = new Event();
        event1.setTitle("First Event");
        event1.setAllDay(true);
        entityManager.persist(event1);

        Event event2 = new Event();
        event2.setTitle("Second Event");
        event2.setAllDay(false);
        entityManager.persist(event2);

        Event event3 = new Event();
        event3.setTitle("Third Event");
        entityManager.persist(event3);

        Iterable<Event> events = eventRepository.findByAllDay(true);
        assertThat(events).hasSize(1).contains(event1);
    }

    @Test
    public void update_event_by_id() {
        // set
        Event event1 = new Event();
        event1.setTitle("Event 1");
        entityManager.persist(event1);

        Event event2 = new Event();
        event2.setTitle("Event 2");
        entityManager.persist(event2);

        // update
        Event updatedEvent = new Event();
        updatedEvent.setTitle("Updated Event");

        Event event = eventRepository.findById(event2.getId()).get();
        event.setTitle(updatedEvent.getTitle());
        eventRepository.save(event);

        // check
        Event checkEvent = eventRepository.findById(event2.getId()).get();
        assertThat(checkEvent.getId()).isNotNull();
        assertThat(checkEvent.getId()).isEqualTo(event2.getId());
        assertThat(checkEvent.getTitle()).isEqualTo(updatedEvent.getTitle());
    }

    @Test
    public void deleteById() {
        Event event1 = new Event();
        event1.setTitle("Event 1");
        entityManager.persist(event1);

        Event event2 = new Event();
        event2.setTitle("Event 2");
        entityManager.persist(event2);

        Event event3 = new Event();
        event3.setTitle("Third Event");
        entityManager.persist(event3);

        eventRepository.deleteById(event2.getId());

        Iterable<Event> events = eventRepository.findAll();
        assertThat(events).hasSize(2).contains(event1, event3);
    }

    @Test
    public void deleteAll() {
        Event event1 = new Event();
        event1.setTitle("Event 1");
        entityManager.persist(event1);

        Event event2 = new Event();
        event2.setTitle("Event 2");
        entityManager.persist(event2);

        eventRepository.deleteAll();
        assertThat(eventRepository.findAll()).isEmpty();
    }
}