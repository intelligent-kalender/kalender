package org.bestname.kalender.controller;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.bestname.kalender.entities.Event;
import org.bestname.kalender.repository.UserRepository;
import org.bestname.kalender.services.EventService;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.RequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import java.util.*;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.when;

/**
 * @author Roland Tömösközi (roland.toemoeskoezi@outlook.com)
 * Created on 23.01.2021
 */
@RunWith(SpringRunner.class)
@WebMvcTest(EventRestController.class)
@WithMockUser(username = "user", password = "user", roles = "user")
public class EventRestControllerTest {
    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private WebApplicationContext webApplicationContext;

    @MockBean
    private EventService eventService;

    @MockBean
    private UserRepository userRepository; // indirectly used for security

    private Event mockEvent;
    private String inputInJson;

    @Before
    public void setUp() throws JsonProcessingException {
        mockMvc = MockMvcBuilders.webAppContextSetup(webApplicationContext).build();

        mockEvent = new Event();
        mockEvent.setId(1L);
        mockEvent.setTitle("TestEvent");
        mockEvent.setEventStart(
                new GregorianCalendar(2020, Calendar.FEBRUARY, 5, 15, 0, 0));
        mockEvent.setEventEnd(
                new GregorianCalendar(2020, Calendar.FEBRUARY, 5, 15, 30, 0));
        mockEvent.setAllDay(false);

        inputInJson = this.mapToJson(mockEvent);
    }

    @Test
    public void createEvent() throws Exception {
        when(eventService.save(Mockito.any(Event.class))).thenReturn(mockEvent);

        RequestBuilder requestBuilder = MockMvcRequestBuilders
                .post("/api/event/create")
                .accept(MediaType.APPLICATION_JSON).content(inputInJson)
                .contentType(MediaType.APPLICATION_JSON);

        // check status
        MvcResult result = mockMvc.perform(requestBuilder).andReturn();
        MockHttpServletResponse response = result.getResponse();
        assertEquals(HttpStatus.CREATED.value(), response.getStatus());

        // check value
        String outputInJson = response.getContentAsString();
        assertThat(outputInJson).isEqualTo(inputInJson);
    }

    @Test
    public void updateEvent() throws Exception {
        mockEvent.setTitle("TestEventUpdated");
        when(eventService.updateEvent(Mockito.anyLong(), Mockito.any(Event.class))).thenReturn(mockEvent);

        RequestBuilder requestBuilder = MockMvcRequestBuilders
                .put("/api/event/update/1")
                .accept(MediaType.APPLICATION_JSON).content(inputInJson)
                .contentType(MediaType.APPLICATION_JSON);

        // check status
        MvcResult result = mockMvc.perform(requestBuilder).andReturn();
        MockHttpServletResponse response = result.getResponse();
        assertEquals(HttpStatus.OK.value(), response.getStatus());

        // check content
        String outputInJson = result.getResponse().getContentAsString();
        String expectedJson = this.mapToJson(mockEvent);
        assertThat(outputInJson).isEqualTo(expectedJson);
    }

    @Test
    public void deleteEvent() throws Exception {
        RequestBuilder requestBuilder = MockMvcRequestBuilders
                .delete("/api/event/deleteById/1")
                .accept(MediaType.APPLICATION_JSON).content(inputInJson)
                .contentType(MediaType.APPLICATION_JSON);

        // check status
        MvcResult result = mockMvc.perform(requestBuilder).andReturn();
        MockHttpServletResponse response = result.getResponse();
        assertEquals(HttpStatus.OK.value(), response.getStatus());
    }

    @Test
    public void getAllEvents() throws Exception {
        Event mockEvent2 = new Event();
        mockEvent2.setId(2L);
        mockEvent2.setTitle("TestEvent_2");
        mockEvent2.setEventStart(
                new GregorianCalendar(2020, Calendar.FEBRUARY, 6, 15, 0, 0));
        mockEvent2.setEventEnd(
                new GregorianCalendar(2020, Calendar.FEBRUARY, 6, 15, 30, 0));
        mockEvent2.setAllDay(false);

        List<Event> eventList = new ArrayList<>();
        eventList.add(mockEvent);
        eventList.add(mockEvent2);

        when(eventService.getAllEvents()).thenReturn(eventList);

        RequestBuilder requestBuilder = MockMvcRequestBuilders
                .get("/api/event/getAllEvents")
                .accept(MediaType.APPLICATION_JSON);

        // check status
        MvcResult result = mockMvc.perform(requestBuilder).andReturn();
        MockHttpServletResponse response = result.getResponse();
        assertEquals(HttpStatus.OK.value(), response.getStatus());

        // check content
        String outputInJson = result.getResponse().getContentAsString();
        String expectedJson = this.mapToJson(eventList);
        assertThat(outputInJson).isEqualTo(expectedJson);
    }

    @Test
    public void eventByTitel() throws Exception {
        when(eventService.getEventByTitle(Mockito.anyString())).thenReturn(mockEvent);

        RequestBuilder requestBuilder = MockMvcRequestBuilders
                .get("/api/event/eventByTitel/TestEvent")
                .accept(MediaType.APPLICATION_JSON);

        // check status
        MvcResult result = mockMvc.perform(requestBuilder).andReturn();
        MockHttpServletResponse response = result.getResponse();
        assertEquals(HttpStatus.OK.value(), response.getStatus());

        // check content
        String outputInJson = result.getResponse().getContentAsString();
        String expectedJson = this.mapToJson(mockEvent);
        assertThat(outputInJson).isEqualTo(expectedJson);
    }

    @Test
    public void eventByAllDay() throws Exception {
        Event mockEvent2 = new Event();
        mockEvent2.setId(2L);
        mockEvent2.setTitle("TestEvent2");
        mockEvent2.setEventStart(
                new GregorianCalendar(2020, Calendar.FEBRUARY, 5, 15, 0, 0));
        mockEvent2.setEventEnd(
                new GregorianCalendar(2020, Calendar.FEBRUARY, 5, 15, 30, 0));
        mockEvent2.setAllDay(true);

        // store events in list
        List<Event> eventList = new ArrayList<>();
        eventList.add(mockEvent);
        eventList.add(mockEvent2);

        when(eventService.findByAllDay(Mockito.anyBoolean())).thenReturn(eventList);

        RequestBuilder requestBuilder = MockMvcRequestBuilders
                .get("/api/event/findByAllDay/true")
                .accept(MediaType.APPLICATION_JSON);

        // check status
        MvcResult result = mockMvc.perform(requestBuilder).andReturn();
        MockHttpServletResponse response = result.getResponse();
        assertEquals(HttpStatus.OK.value(), response.getStatus());

        // check content
        String expectedJson = this.mapToJson(eventList);
        String outputInJson = result.getResponse().getContentAsString();
        assertThat(outputInJson).isEqualTo(expectedJson);
    }

    @Test
    public void getEventById() throws Exception {
        when(eventService.getOne(Mockito.anyLong())).thenReturn(mockEvent);

        RequestBuilder requestBuilder = MockMvcRequestBuilders
                .get("/api/event/getOne/1")
                .accept(MediaType.APPLICATION_JSON);

        // check status
        MvcResult result = mockMvc.perform(requestBuilder).andReturn();
        MockHttpServletResponse response = result.getResponse();
        assertEquals(HttpStatus.OK.value(), response.getStatus());

        // check content
        String expectedJson = this.mapToJson(mockEvent);
        String outputInJson = result.getResponse().getContentAsString();
        assertThat(outputInJson).isEqualTo(expectedJson);
    }

    /**
     * Maps an Object into a JSON String. Uses a Jackson ObjectMapper.
     */
    private String mapToJson(Object object) throws JsonProcessingException {
        ObjectMapper objectMapper = new ObjectMapper();
        return objectMapper.writeValueAsString(object);
    }
}