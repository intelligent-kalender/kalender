package org.bestname.kalender.service;

import org.bestname.kalender.entities.Event;
import org.bestname.kalender.repository.EventRepository;
import org.bestname.kalender.services.EventService;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.*;

/**
 * @author Roland Tömösközi (roland.toemoeskoezi@outlook.com)
 * Created on 29.01.2021
 */
@RunWith(SpringRunner.class)
@SpringBootTest
@ActiveProfiles("test")
public class EventServiceTest {

    @MockBean
    private EventRepository eventRepository;

    @Autowired
    private EventService eventService;

    private Event testEvent;

    @Before
    public void setUp() {
        testEvent = new Event();
        testEvent.setId(1L);
        testEvent.setTitle("zahnartzt");
        testEvent.setEventStart(
                new GregorianCalendar(2020, Calendar.FEBRUARY, 5, 15, 0, 0));
        testEvent.setEventEnd(
                new GregorianCalendar(2020, Calendar.FEBRUARY, 5, 15, 30, 0));
        testEvent.setAllDay(false);
    }

    @Test
    public void save() {
        when(eventRepository.save(Mockito.any(Event.class))).thenReturn(testEvent);
        assertThat(eventService.save(testEvent)).isEqualTo(testEvent);
    }

    @Test
    public void deleteById() {
        // find
        when(eventRepository.getOne(Mockito.anyLong())).thenReturn(testEvent);
        // delete
        eventService.deleteById(1L);
        // verify
        verify(eventRepository, times(1)).deleteById(testEvent.getId());
    }

    @Test
    public void getAllEvents() {
        Event testEvent2 = new Event();
        testEvent2.setTitle("zahnartzt");
        testEvent2.setEventStart(
                new GregorianCalendar(2020, Calendar.FEBRUARY, 15, 0, 0, 0));
        testEvent2.setEventEnd(
                new GregorianCalendar(2020, Calendar.FEBRUARY, 16, 0, 0, 0));
        testEvent2.setAllDay(true);

        List<Event> eventList = new ArrayList<>();
        eventList.add(testEvent);
        eventList.add(testEvent2);

        when(eventRepository.findAll()).thenReturn(eventList);
        assertThat(eventService.getAllEvents()).isEqualTo(eventList);
    }

    @Test
    public void getEventByTitle() {
        when(eventRepository.findByTitle(Mockito.anyString())).thenReturn(testEvent);
        assertThat(eventService.getEventByTitle(testEvent.getTitle())).isEqualTo(testEvent);
    }

    @Test
    public void findByAllDay() {
        Event testEvent2 = new Event();
        testEvent2.setTitle("zahnartzt");
        testEvent2.setEventStart(
                new GregorianCalendar(2020, Calendar.FEBRUARY, 15, 0, 0, 0));
        testEvent2.setEventEnd(
                new GregorianCalendar(2020, Calendar.FEBRUARY, 16, 0, 0, 0));
        testEvent2.setAllDay(true);

        List<Event> eventList = new ArrayList<>();
        eventList.add(testEvent);
        eventList.add(testEvent2);

        when(eventRepository.findByAllDay(Mockito.anyBoolean())).thenReturn(eventList);
        assertThat(eventService.findByAllDay(testEvent.isAllDay())).isEqualTo(eventList);
    }

    @Test
    public void getOne() {
        when(eventRepository.getOne(Mockito.anyLong())).thenReturn(testEvent);
        assertThat(eventService.getOne(testEvent.getId())).isEqualTo(testEvent);
    }

    @Test
    public void updateEvent() {
        when(eventRepository.getOne(Mockito.anyLong())).thenReturn(testEvent);

        // update
        String title_updated = "TestEvent_updated";
        GregorianCalendar eventStart_updated =
                new GregorianCalendar(2021, Calendar.FEBRUARY, 2, 15, 0, 0);
        GregorianCalendar eventEnd_updated =
                new GregorianCalendar(2021, Calendar.FEBRUARY, 2, 30, 0, 0);

        testEvent.setTitle(title_updated);
        testEvent.setEventStart(eventStart_updated);
        testEvent.setEventEnd(eventEnd_updated);
        testEvent.setAllDay(true);

        eventService.updateEvent(1L, testEvent);

        assertThat(eventService.getOne(1L).getTitle()).isEqualTo(title_updated);
        assertThat(eventService.getOne(1L).getEventStart()).isEqualTo(eventStart_updated);
        assertThat(eventService.getOne(1L).getEventEnd()).isEqualTo(eventEnd_updated);
        assertThat(eventService.getOne(1L).isAllDay()).isEqualTo(true);
    }
}