package org.bestname.kalender.service;

import org.bestname.kalender.entities.User;
import org.bestname.kalender.repository.UserRepository;
import org.bestname.kalender.services.UserDetailsServiceImpl;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Bean;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.test.context.junit4.SpringRunner;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.when;

/**
 * @author Roland Tömösközi (roland.toemoeskoezi@outlook.com)
 * Created on 08.01.2021
 */
@RunWith(SpringRunner.class)
public class UserDetailsServiceImplIT {

    @TestConfiguration
    static class UserDetailsServiceImplTestContextConfiguration {
        @Bean
        public UserDetailsServiceImpl userDetailsService() {
            return new UserDetailsServiceImpl();
        }
    }

    @Autowired
    private UserDetailsServiceImpl userDetailsService;

    @MockBean
    private UserRepository userRepository;

    @Before
    public void setup() {
        User mustermann = new User();
        mustermann.setUsername("mustermann");
        mustermann.setPassword("foobar");
        mustermann.setRole("ROLE_USER");
        mustermann.setEnabled(true);
        mustermann.setFullName("Herr Mustermann");
        mustermann.setEmail("mustermann@gmail.com");
        mustermann.setPhoneNumber("+436601234567");

        when(userRepository.getUserByUsername(mustermann.getUsername())).thenReturn(mustermann);
    }

    // TODO: getAuthorities()

    @Test
    public void getUsername_valid() {
        String validUsername = "mustermann";
        UserDetails found = userDetailsService.loadUserByUsername(validUsername);

        assertThat(found.getUsername()).isEqualTo(validUsername);
    }

    @Test(expected = UsernameNotFoundException.class)
    public void getUsername_invalid() {
        String invalidUsername = "musterfrau";
        userDetailsService.loadUserByUsername(invalidUsername);
    }

    @Test
    public void getPassword_valid() {
        String validUserName = "mustermann";
        String validPassword = "foobar";
        UserDetails found = userDetailsService.loadUserByUsername(validUserName);
        assertThat(found.getPassword()).isEqualTo(validPassword);
    }

    @Test
    public void getPassword_invalid() {
        String validUserName = "mustermann";
        String invalidPassword = "musterpassword";
        UserDetails found = userDetailsService.loadUserByUsername(validUserName);
        assertThat(found.getPassword()).isNotEqualTo(invalidPassword);
    }

    @Test
    public void isAccountNonExpired() {
        String validUserName = "mustermann";
        UserDetails found = userDetailsService.loadUserByUsername(validUserName);
        assertThat(found.isAccountNonExpired()).isTrue();
    }

    @Test
    public void isAccountNonLocked() {
        String validUserName = "mustermann";
        UserDetails found = userDetailsService.loadUserByUsername(validUserName);
        assertThat(found.isAccountNonLocked()).isTrue();
    }

    @Test
    public void isCredentialsNonExpired() {
        String validUserName = "mustermann";
        UserDetails found = userDetailsService.loadUserByUsername(validUserName);
        assertThat(found.isCredentialsNonExpired()).isTrue();
    }

    @Test
    public void isEnabled() {
        String validUserName = "mustermann";
        UserDetails found = userDetailsService.loadUserByUsername(validUserName);
        assertThat(found.isEnabled()).isTrue();
    }
}