package org.bestname.kalender.controller;

import org.bestname.kalender.entities.Event;
import org.bestname.kalender.services.EventService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * @author Roland Tömösközi (roland.toemoeskoezi@outlook.com)
 * Created on 12.01.2021
 */
@RestController
@RequestMapping("/api/event")
public class EventRestController {
    private EventService eventService;

    @Autowired
    public void setEventService(EventService eventService) {
        this.eventService = eventService;
    }

    @PostMapping(value = "/create",
            consumes = MediaType.APPLICATION_JSON_VALUE,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseStatus(HttpStatus.CREATED)
    public Event create(@RequestBody Event event) {
        return eventService.save(event);
    }

    @PutMapping(value = "update/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseStatus(HttpStatus.OK)
    public Event updateEvent(@PathVariable Long id, @RequestBody Event newEvent) {
        return eventService.updateEvent(id, newEvent);
    }

    @DeleteMapping("deleteById/{id}")
    public void deleteById(@PathVariable Long id) {
        eventService.deleteById(id);
    }

    @GetMapping("/getAllEvents")
    public List<Event> getAllEvents() {
        return eventService.getAllEvents();
    }

    @GetMapping("/eventByTitel/{titel}")
    public Event eventByTitel(@PathVariable(value = "titel") String title) {
        return eventService.getEventByTitle(title);
    }

    @GetMapping("/findByAllDay/{allDay}")
    public List<Event> findByAllDay(@PathVariable(value = "allDay") boolean allDay) {
        return eventService.findByAllDay(allDay);
    }

    @GetMapping(value = "/getOne/{id}")
    public Event getOne(@PathVariable(value = "id") Long id) {
        return eventService.getOne(id);
    }
}