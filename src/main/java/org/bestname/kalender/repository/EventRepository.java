package org.bestname.kalender.repository;

import org.bestname.kalender.entities.Event;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * @author Roland Tömösközi (roland.toemoeskoezi@outlook.com)
 * Created on 03.01.2021
 */
@Repository
public interface EventRepository extends JpaRepository<Event, Long> {
    Event findByTitle(@Param("title") String title);
    List<Event> findByAllDay(boolean allDay);
}