package org.bestname.kalender.repository;

import org.bestname.kalender.entities.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

/**
 * @author Roland Tömösközi (roland.toemoeskoezi@outlook.com)
 * Created on 04.01.2021
 */
public interface UserRepository extends JpaRepository<User, Long> {

    @Query("SELECT u FROM User u WHERE u.username = :username")
    User getUserByUsername(@Param("username") String username);
}
