package org.bestname.kalender.entities;

import lombok.Data;

import javax.persistence.*;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Pattern;

/**
 * @author Roland Tömösközi (roland.toemoeskoezi@outlook.com)
 * Created on 04.01.2021
 */
@Entity
@Table(name = "USERS")
@Data
public class User {
    private static final long serialVersionUID = 1L;

    @Id
    @Column(name = "user_id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotBlank
    private String username;

    @NotBlank
    private String password;

    private String role;
    private boolean enabled;

    @Pattern(regexp = "^[\\p{L} .'-]+$")
    private String fullName;

    @Email
    private String email;

    @Pattern(regexp = "^[+]*[(]{0,1}[0-9]{1,4}[)]{0,1}[-\\s\\./0-9]*$")
    private String phoneNumber;
}