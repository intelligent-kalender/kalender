package org.bestname.kalender.services;

import org.bestname.kalender.entities.Event;
import org.bestname.kalender.repository.EventRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @author Roland Tömösközi (roland.toemoeskoezi@outlook.com)
 * Created on 13.01.2021
 */
@Service
public class EventService {
    private EventRepository eventRepo;

    @Autowired
    public void setEventRepo(EventRepository eventRepo) {
        this.eventRepo = eventRepo;
    }

    public Event save(Event event) {
        return eventRepo.save(event);
    }

    public void deleteById(Long id) {
        eventRepo.deleteById(id);
    }

    public List<Event> getAllEvents() {
        return eventRepo.findAll();
    }

    public Event getEventByTitle(String title) {
        return eventRepo.findByTitle(title);
    }

    public List<Event> findByAllDay(boolean allDay) {
        return eventRepo.findByAllDay(allDay);
    }

    public Event getOne(Long id) {
        return eventRepo.getOne(id);
    }

    public Event updateEvent(Long id, Event newEvent) {
        if (eventRepo.findById(id).isPresent()) {
            // find event
            Event existingEvent = eventRepo.findById(id).get();

            // update from parameters
            existingEvent.setTitle(newEvent.getTitle());
            existingEvent.setEventStart(newEvent.getEventStart());
            existingEvent.setEventEnd(newEvent.getEventEnd());
            existingEvent.setAllDay(newEvent.isAllDay());

            // save and return updated event
            return eventRepo.save(existingEvent);
        } else {
            return null;
        }
    }
}
