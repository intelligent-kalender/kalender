## Project Status:
- Spring Security - Authentication (decrypted)
- Spring JPA - Hibernate
- REST API
- Stages: test / dev
- Tests: 26 (JUnit / Integration)
- Documentation: Atlassian Confluence
- Project management: Atlassian Jira

## Next steps:
- Registration
- Business logic (calender)
- Firebase (Firestore -> Cloud Database, Hosting)
- Authentication update: OAuth 2.0 - JWT / Firebase
---

## Maven Project check
If the IDEA does not recognize the project as a Maven project, you have to set it manually.
- Click with right mouse on **pom.xml** than choose **add as maven project**.
---

## Start Database Server
1. Open command prompt (**cmd**) and go to **C:\Entwicklung\tools\db-derby-10.15.2.0-bin\bin**
2. Write the following command: **startNetworkServer**
---

## Connect to Database
1. Click **Database** on the right side.
2. Click on **plus symbol**, then on Data Source and select the Apache Derby database.
3. In the General select the **Apache Derby Remote** as driver.
4. Fill the following fields like this:
    1. **Name:** Derby
    2. **Comment:** Hibernate
    3. **Host:** localhost
    4. **Port:** 1527
    5. **User:** kalender
    6. **URL:** jdbc:derby://localhost:1527/kalender;create=true
5. **Test** the connection
---

## Check entity
1. Select an **entity** and check if the **name** of the column/table has the correct **data soruce**.
   If they are not correct, change them.
---

## Maven
1. Click **Maven** on the right side.
2. Select your maven project than click on **Lifecycle**.
3. Select **clean** and **install** from the list and **run** it.
---

## Register users in DB
INSERT into USERS (USER_ID, USERNAME, PASSWORD, ROLE, ENABLED, FULL_NAME, EMAIL, PHONE_NUMBER)
VALUES (1,
'user',
'$2a$04$fn1rMg4gvDLWStgK9Wcwb.ZYL3W3gOjstMvsdR.AWpNM3VudFi4Eu',
'ROLE_USER',
true,
'Musteruser',
'mustermann@gmail.com',
'+436601234567');

INSERT into USERS (USER_ID, USERNAME, PASSWORD, ROLE, ENABLED, FULL_NAME, EMAIL, PHONE_NUMBER)
VALUES (2,
'admin',
'$2a$04$O.OWW73jbSK6K8hx/hm.y.kh3vaVYlMVmRaVSFfZsV2NFD7S8pM5C',
'ROLE_ADMIN',
true,
'Mustermann',
'mustermann@gmail.com',
'+436601234567');
---

## Run the application
1. Click on the **green triangle**
2. Open your browser with the following link: **http://localhost:8080**
3. **Log in:**
    1. **User:** user / admin
    2. **Password:** user / admin
